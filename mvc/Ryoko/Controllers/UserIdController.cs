﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Ryoko.Controllers
{
    public class UserIdController : Controller
    {
        public string UserId()
        {
            return User.Identity.GetUserId();
        }
    }
}
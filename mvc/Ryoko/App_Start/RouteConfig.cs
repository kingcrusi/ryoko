﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ryoko
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "About",
                url: "about",
                defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
                name: "References",
                url: "references",
                defaults: new { controller = "Home", action = "References" }
            );
            /*
            routes.MapRoute(
                name: "ReviewExpenses",
                url: "expense/review",
                defaults: new { controller = "Expense", action = "Review" }
            );

            routes.MapRoute(
                name: "AddExpense",
                url: "expense/add",
                defaults: new { controller = "Expense", action = "Add" }
            );
            */

            routes.MapRoute(
                name: "UserExpenses",
                url: "expense/user/{userid}",
                defaults: new { controller = "Expense", action = "Get"}
            );

            routes.MapRoute(
                name: "UserId",
                url: "userid",
                defaults: new { controller = "UserId", action = "UserId" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
